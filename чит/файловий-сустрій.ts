import { existsSync, mkdirSync } from "fs";
import { dirname } from "path";

export function ручитиИснуванняПутіФайлаОБ(путь: string) {
  const путьПапки = dirname(путь);
  ручитиИснуванняПутіПапкиОБ(путьПапки);
}

export function ручитиИснуванняПутіПапкиОБ(путьПапки: string) {
  if (!existsSync(путьПапки)) {
    mkdirSync(путьПапки, { recursive: true });
  }
}
