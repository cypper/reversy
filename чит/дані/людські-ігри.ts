import { Гра, Події, Хід } from "../гра/гра";
import { ГравецьПрописаний } from "../гра/гравці";
import ігри from "../дані/othello_dataset.json";

// async function витягЗCSV(файл: string): Promise<string[][]> {
//   const parser = parse(readFileSync(файл));

//   const records: string[][] = [];
//   let чиЗагловок = true;

//   return new Promise((resolve, reject) => {
//     // Use the readable stream api to consume records
//     parser.on("readable", function () {
//       let record;
//       while ((record = parser.read()) !== null) {
//         if (чиЗагловок) {
//           чиЗагловок = false;
//         } else {
//           records.push(record);
//         }
//       }
//     });
//     parser.on("error", () => reject("ПОмилка"));
//     parser.on("end", () => resolve(records));
//   });
// }
// async function csvВJSON() {
//   const ігри = await витягЗCSV("чит/дані/othello_dataset.csv");
//   writeFileSync("чит/дані/othello_dataset.json", JSON.stringify(ігри));
// }
// csvВJSON()

const adsf = "abcdefgh";

function ходиЗСтрічки(стрічка: string): Хід[] {
  const ходи: Хід[] = [];

  for (let index = 0; index < стрічка.length; index += 2) {
    const хідСтрічкою = стрічка.slice(index, index + 2);

    ходи.push({
      x: Number(хідСтрічкою[1]) - 1,
      y: adsf.indexOf(хідСтрічкою[0]),
    });
  }

  return ходи;
}

export async function провестиВипадковуЛюдськуГру(події?: Події) {
  const random = Math.floor(Math.random() * ігри.length);
  const гра = ігри[random];

  const ходи = ходиЗСтрічки(гра[2]);

  const гр1 = new ГравецьПрописаний(ходи);
  const гр2 = new ГравецьПрописаний(ходи);

  return await new Гра(8, 8).провестиГру(гр1, гр2, події);
}
