import { Interface } from "readline";
import { Хід } from "./гра";
import { Поле, СтанГравця, СтанКлітинки } from "./поле";

export class Оболонка {
  static async отриматиХідЛюдини(поле: Поле, станГравця: СтанГравця) {
    if (Оболонка.isNode()) {
      return Оболонка.отриматиХідЛюдиниNode(поле, станГравця);
    } else {
      return Оболонка.отриматиХідЛюдиниЛубач(поле);
    }
  }
  private static отриматиХідЛюдиниЛубач(поле: Поле) {
    return new Promise<Хід>((res) => {
      поле.поКлітинкамЗворотнєX((x, y) => {
        const стан = поле.витягСтануКлітинки(x, y);

        if (стан === СтанКлітинки.ПУСТА) {
          const кліт = document.getElementById("клітина" + x + "_" + y);
          кліт?.addEventListener("click", () => {
            res({ x, y });
          });
        }
      });
    });
  }

  private static async отриматиХідЛюдиниNode(
    поле: Поле,
    станГравця: СтанГравця,
  ) {
    const rl = (await require("readline")).createInterface(
      process.stdin,
      process.stdout,
    ) as Interface;

    return new Promise<Хід>((res) => {
      rl.setPrompt(`ваш хід (ваш стан клітинки ${станГравця}?\n`);
      rl.prompt();
      rl.on("line", async (пись) => {
        const [x, y] = пись.split(" ").map((n) => parseInt(n));

        if (
          typeof x !== "number" ||
          typeof y !== "number" ||
          isNaN(x) ||
          isNaN(y)
        ) {
          console.log("Невірно введені дані, попробуйте ще раз");
          rl.close();
          res(await Оболонка.отриматиХідЛюдиниNode(поле, станГравця));
        } else {
          rl.close();
          res({ x, y });
        }
      });
    });
  }

  static вивід(поле: Поле) {
    if (Оболонка.isNode()) {
      this.вивідNode(поле);
    } else {
      this.вивідЛубач(поле);
    }
  }

  private static вивідNode(поле: Поле) {
    console.log("------");
    console.log(
      "   " +
        Array(поле.довжинаY)
          .fill(null)
          .map((_, вк) => `${вк}`)
          .join(" "),
    );
    console.log("");

    for (let x = 0; x < поле.довжинаX; x++) {
      let ряд = `${x} `;
      for (let y = 0; y < поле.довжинаY; y++) {
        const стан = поле.витягСтануКлітинки(x, y);
        if (стан === СтанКлітинки.ПУСТА) ряд += " ·";
        if (стан === СтанКлітинки.ЧОРНЕ) ряд += " ч";
        if (стан === СтанКлітинки.БІЛЕ) ряд += " б";
      }
      console.log(ряд);
    }
    console.log("------");
  }

  private static вивідЛубач(поле: Поле) {
    const гол = document.getElementById("гра");
    if (!гол) throw new Error("Нема де відображати поле");
    гол.innerHTML = "";

    for (let x = поле.довжинаX - 1; x >= 0; x--) {
      const ряд = document.createElement("div");
      ряд.id = "ряд" + x;
      for (let y = 0; y < поле.довжинаY; y++) {
        const кліт = document.createElement("div");
        кліт.id = "клітина" + x + "_" + y;
        ряд.appendChild(кліт);
      }
      гол.appendChild(ряд);
    }

    поле.поКлітинкамЗворотнєX((x, y) => {
      const кліт = document.getElementById("клітина" + x + "_" + y);
      if (!кліт) throw new Error("Нема клітинки");
      const стан = поле.витягСтануКлітинки(x, y);
      if (стан === СтанКлітинки.ПУСТА) кліт.innerHTML = " ·";
      if (стан === СтанКлітинки.ЧОРНЕ) кліт.innerHTML = " 1";
      if (стан === СтанКлітинки.БІЛЕ) кліт.innerHTML = " 2";
    });
  }

  private static isNode() {
    return typeof process === "object";
  }
}
