import { Місце, Хід } from "./гра";

export enum СтанКлітинки {
  ПУСТА = "0",
  ЧОРНЕ = "ч",
  БІЛЕ = "б",
}

export type СтанГравця = СтанКлітинки.ЧОРНЕ | СтанКлітинки.БІЛЕ;
export type Клітинка<T extends СтанКлітинки = СтанКлітинки> = [
  number,
  number,
  T,
];

export type РядницяПеревертів = Клітинка[][][];

export class Поле {
  поле: Array<Array<СтанКлітинки>>; // твар - [this.довжинаX, this.довжинаY]
  constructor(
    readonly довжинаX: number,
    readonly довжинаY: number,
  ) {
    this.поле = Array(this.довжинаX)
      .fill(null)
      .map(() => Array(this.довжинаY).fill(0));
    this.поКлітинкам((x, y) =>
      this.поставитиСтанКлітинки(x, y, СтанКлітинки.ПУСТА),
    );

    // чотири на середині
    this.поставитиСтанКлітинки(
      Math.floor(this.довжинаX / 2 - 1),
      Math.floor(this.довжинаY / 2 - 1),
      СтанКлітинки.БІЛЕ,
    );
    this.поставитиСтанКлітинки(
      Math.floor(this.довжинаX / 2),
      Math.floor(this.довжинаY / 2),
      СтанКлітинки.БІЛЕ,
    );
    this.поставитиСтанКлітинки(
      Math.floor(this.довжинаX / 2),
      Math.floor(this.довжинаY / 2 - 1),
      СтанКлітинки.ЧОРНЕ,
    );
    this.поставитиСтанКлітинки(
      Math.floor(this.довжинаX / 2 - 1),
      Math.floor(this.довжинаY / 2),
      СтанКлітинки.ЧОРНЕ,
    );
  }

  ціглийТезярСтануПоля() {
    let тезяр = "";
    for (let x = 0; x < this.довжинаX; x++) {
      for (let y = 0; y < this.довжинаY; y++) {
        // тезяр += ((x + 1) * this.довжинаX)*((y + 1) * 3) + (this.поле[x][y] === СтанКлітинки.ПУСТА ? 0 : this.поле[x][y] === СтанКлітинки.ЧОРНЕ ? 1 : 2)
        тезяр +=
          this.поле[x][y] === СтанКлітинки.ПУСТА
            ? "0"
            : this.поле[x][y] === СтанКлітинки.ЧОРНЕ
              ? "1"
              : "2";
      }
    }
    return тезяр;
  }

  чиПозаМежами(x: number, y: number) {
    return 0 > x || x >= this.довжинаX || 0 > y || y >= this.довжинаY;
  }

  витягСтануКлітинки(x: number, y: number) {
    return this.поле[x][y];
  }

  витягКлітинки(x: number, y: number): Клітинка | null {
    return this.чиПозаМежами(x, y)
      ? null
      : [x, y, this.витягСтануКлітинки(x, y)];
  }

  витягСусіднихКлітинокЗСтаном<T extends СтанКлітинки>(
    x: number,
    y: number,
    стан: T,
  ) {
    const сусідніКл = [
      this.витягКлітинки(x + 1, y),
      this.витягКлітинки(x, y + 1),
      this.витягКлітинки(x - 1, y),
      this.витягКлітинки(x, y - 1),
      this.витягКлітинки(x + 1, y + 1),
      this.витягКлітинки(x - 1, y - 1),
      this.витягКлітинки(x + 1, y - 1),
      this.витягКлітинки(x - 1, y + 1),
    ];
    return сусідніКл.filter((кл) => кл?.[2] === стан) as Клітинка<T>[];
  }

  поставитиСтанКлітинки(x: number, y: number, стан: СтанКлітинки) {
    this.поле[x][y] = стан;
  }

  вСтанСуперника(станГравця: СтанГравця) {
    return станГравця === СтанКлітинки.ЧОРНЕ
      ? СтанКлітинки.БІЛЕ
      : СтанКлітинки.ЧОРНЕ;
  }

  пустіКлітинки(): Місце[] {
    return this.поле.flatMap((ряд, x) => {
      return ряд
        .map((кліт, y) => {
          return кліт === СтанКлітинки.ПУСТА ? { x, y } : null;
        })
        .filter((м) => м) as Місце[];
    });
  }

  поКлітинкам(по: (x: number, y: number) => void) {
    for (let x = 0; x < this.довжинаX; x++) {
      for (let y = 0; y < this.довжинаY; y++) {
        по(x, y);
      }
    }
  }

  поКлітинкамВислід<T>(по: (x: number, y: number) => T): T[] {
    const висліди = [];
    for (let x = 0; x < this.довжинаX; x++) {
      for (let y = 0; y < this.довжинаY; y++) {
        висліди.push(по(x, y));
      }
    }
    return висліди;
  }

  поПустимКлітинкам(по: (x: number, y: number) => void) {
    for (let x = 0; x < this.довжинаX; x++) {
      for (let y = 0; y < this.довжинаY; y++) {
        if (this.витягСтануКлітинки(x, y) === СтанКлітинки.ПУСТА) по(x, y);
      }
    }
  }

  // поМожливимХодамГравця(станГравця: СтанГравця, по: (x: number, y: number) => void) {
  //   this.вМожливіХодиГравця(станГравця).map((ряд, x) => {
  //     ряд.map((перевернуті, y) => {
  //       if (перевернуті.length > 0) по(x, y, перевернуті);
  //     })
  //   });
  //   // for (let x = 0; x < this.довжинаX; x++) {
  //   //   for (let y = 0; y < this.довжинаY; y++) {
  //   //     if (this.витягСтануКлітинки(x, y) === СтанКлітинки.ПУСТА) по(x, y);
  //   //   }
  //   // }
  // }

  рядницяПоСтану(стан: СтанКлітинки) {
    return this.поле.map((ряд) => {
      return ряд.map((кліт) => {
        return кліт === стан ? 1 : 0;
      });
    });
  }

  вРядницюПеревертівГравця(станГравця: СтанГравця) {
    const станСуперника = this.вСтанСуперника(станГравця);
    const рядницяГравця = this.рядницяПоСтану(станГравця);
    const рядницяСуперника = this.рядницяПоСтану(станСуперника);

    const можливіХоди: number[][] = Array(this.довжинаX)
      .fill(null)
      .map(() => Array(this.довжинаY).fill(0));

    // можливі ходи без врахування положення гравців та перевернутих клітинок
    for (let x = 0; x < this.довжинаX; x++) {
      for (let y = 0; y < this.довжинаY; y++) {
        зсуви: for (let зx = -1; зx <= 1; зx++) {
          for (let зy = -1; зy <= 1; зy++) {
            if ((рядницяСуперника[x - зx] ?? [])[y - зy] === 1) {
              можливіХоди[x][y] = 1;
              break зсуви;
            }
          }
        }
        // можливіХоди[x][y] = (рядницяСуперника[x] ?? [])[y]
        //   || (рядницяСуперника[x - 1] ?? [])[y - 0]
        //   || (рядницяСуперника[x - 0] ?? [])[y - 1]
        //   || (рядницяСуперника[x - 0] ?? [])[y + 1]
        //   || (рядницяСуперника[x + 1] ?? [])[y - 0]
        //   || (рядницяСуперника[x + 1] ?? [])[y + 1]
        //   || (рядницяСуперника[x - 1] ?? [])[y + 1]
        //   || (рядницяСуперника[x + 1] ?? [])[y - 1]
        //   || (рядницяСуперника[x - 1] ?? [])[y - 1]
        //   || 0
      }
    }

    // враховуємо положення гравців
    for (let x = 0; x < this.довжинаX; x++) {
      for (let y = 0; y < this.довжинаY; y++) {
        можливіХоди[x][y] >>= рядницяГравця[x][y] || рядницяСуперника[x][y];
      }
    }

    // враховуємо перевернуті клітинки
    const рядникцяПеревертів: РядницяПеревертів = Array(this.довжинаX)
      .fill(null)
      .map(() => Array(this.довжинаY).fill([]));
    for (let x = 0; x < this.довжинаX; x++) {
      for (let y = 0; y < this.довжинаY; y++) {
        if (можливіХоди[x][y] === 0) {
          рядникцяПеревертів[x][y] = [];
          continue;
        }
        const перевернутіКлітинки = [];

        for (let зx = -1; зx <= 1; зx++) {
          for (let зy = -1; зy <= 1; зy++) {
            // if (зx === зy && зy === 0) continue;

            const можливіПеревернутіКлітинки = [];
            for (let i = 1; i < this.найдовшийРяд; i++) {
              const настX = x + зx * i;
              const настY = y + зy * i;
              const настКл = this.витягКлітинки(настX, настY);

              if (настКл?.[2] === станСуперника) {
                можливіПеревернутіКлітинки.push(настКл);
              } else {
                if (
                  настКл?.[2] === станГравця &&
                  можливіПеревернутіКлітинки.length > 0
                ) {
                  перевернутіКлітинки.push(...можливіПеревернутіКлітинки);
                }

                break;
              }
            }
          }
        }

        рядникцяПеревертів[x][y] = перевернутіКлітинки;
      }
    }

    return рядникцяПеревертів;
  }

  рядницюПеревертівВЙмовірніХоди(рядникцяПеревертів: РядницяПеревертів): Хід[] {
    const ймовірніХоди = [];
    for (let x = 0; x < this.довжинаX; x++) {
      for (let y = 0; y < this.довжинаY; y++) {
        if (рядникцяПеревертів[x][y].length > 0) ймовірніХоди.push({ x, y });
      }
    }
    return ймовірніХоди;
  }

  ймовірніХоди(станГравця: СтанГравця) {
    const рядницяПеревертівГравця = this.вРядницюПеревертівГравця(станГравця);
    const ймовірніХоди = this.рядницюПеревертівВЙмовірніХоди(
      рядницяПеревертівГравця,
    );

    return ймовірніХоди;
  }

  ймовірніХодиХухрені(рядницяПеревертівГравця: РядницяПеревертів) {
    const ймовірніХоди = this.рядницюПеревертівВЙмовірніХоди(
      рядницяПеревертівГравця,
    );

    ймовірніХоди.sort((х1, х2) => {
      // const очки1 = рядницяПеревертівГравця[х1.x][х1.y].length;
      // const очки2 = рядницяПеревертівГравця[х2.x][х2.y].length;
      // const очки11 = чиКрай(х1.x, х1.y, this.довжинаX, this.довжинаY)
      //   ? чиКут(х1.x, х1.y, this.довжинаX, this.довжинаY)
      //     ? 25
      //     : 4
      //   : 0;
      // const очки22 = чиКрай(х2.x, х2.y, this.довжинаX, this.довжинаY)
      //   ? чиКут(х2.x, х2.y, this.довжинаX, this.довжинаY)
      //     ? 25
      //     : 4
      //   : 0;
      // return очки2 + очки22 - (очки1 + очки11);

      return СТІЙНОСТІ_КЛІТИНКИ[х2.x][х2.y] - СТІЙНОСТІ_КЛІТИНКИ[х1.x][х1.y];
    });

    return ймовірніХоди;
  }

  поКлітинкамЗворотнєX(по: (x: number, y: number) => void) {
    for (let x = this.довжинаX - 1; x >= 0; x--) {
      for (let y = 0; y < this.довжинаY; y++) {
        по(x, y);
      }
    }
  }

  get кількістьКлітинок() {
    return this.довжинаX * this.довжинаY;
  }

  кількістьКлітинокЗСтаном(стан: СтанКлітинки) {
    let кількість = 0;
    for (let x = 0; x < this.довжинаX; x++) {
      for (let y = 0; y < this.довжинаY; y++) {
        if (this.витягСтануКлітинки(x, y) === стан) кількість++;
      }
    }
    return кількість;
  }

  зісколити() {
    const сколок = new Поле(this.довжинаX, this.довжинаY);
    сколок.поле = this.поле.map((р) => [...р]);
    return сколок;
  }

  get найдовшийРяд() {
    return Math.max(this.довжинаX, this.довжинаY);
  }

  // неповніОчки(станГравця: СтанГравця) {
  //   const станСуперника = this.вСтанСуперника(станГравця);

  //   let оц = 0;
  //   for (let x = 0; x < this.довжинаX; x++) {
  //     for (let y = 0; y < this.довжинаY; y++) {
  //       switch (this.витягСтануКлітинки(x, y)) {
  //         case станГравця:
  //           оц++;
  //           break;
  //         case станСуперника:
  //           оц--;
  //           break;
  //       }
  //     }
  //   }
  //   return оц;
  // }

  неповніОчки(станГравця: СтанГравця) {
    const станСуперника = this.вСтанСуперника(станГравця);

    let оц = 0;
    for (let x = 0; x < this.довжинаX; x++) {
      for (let y = 0; y < this.довжинаY; y++) {
        switch (this.витягСтануКлітинки(x, y)) {
          case станГравця:
            оц += СТІЙНОСТІ_КЛІТИНКИ[x][y] as number;
            break;
          case станСуперника:
            оц -= СТІЙНОСТІ_КЛІТИНКИ[x][y] as number;
            break;
        }
      }
    }
    return оц;
  }

  // неповніОчки(станГравця: СтанГравця) {
  //   return (this.кількістьКлітинокЗСтаном(станГравця));
  // }

  // неповніОчки(станГравця: СтанГравця) {
  //   const оцнки = this.поКлітинкамВислід((x, y) => {
  //     if (this.витягСтануКлітинки(x, y) !== станГравця) return 0;
  //     if (чиКут(x, y, this.довжинаX, this.довжинаY)) return 50;
  //     if (чиКрай(x, y, this.довжинаX, this.довжинаY)) return 15;
  //     return 1;
  //   });
  //   const зоцінка =
  //     (10 - Math.min(10, this.ймовірніХоди(станГравця).length)) * 10;

  //   return оцнки.reduce((гал, оц) => гал + оц, 0 as number) - зоцінка;
  // }
}

const СТІЙНОСТІ_КЛІТИНКИ = [
  [8, -2, 2, 1, 1, 2, -2, 8],
  [-2, -4, -1, -1, -1, -1, -4, -2],
  [2, -1, 1, 0, 0, 1, -1, 2],
  [1, -1, 0, 0.5, 0.5, 0, -1, 1],
  [1, -1, 0, 0.5, 0.5, 0, -1, 1],
  [2, -1, 1, 0, 0, 1, -1, 2],
  [-2, -4, -1, -1, -1, -1, -4, -2],
  [8, -2, 2, 1, 1, 2, -2, 8],
];

export function чиКрай(
  x: number,
  y: number,
  довжинаX: number,
  довжинаY: number,
) {
  return x === 0 || y === 0 || x === довжинаX - 1 || y === довжинаY - 1;
}

export function чиКут(
  x: number,
  y: number,
  довжинаX: number,
  довжинаY: number,
) {
  return (
    чиКрай(x, y, довжинаX, довжинаY) &&
    (x === y || x === y - (довжинаY - 1) || y === x - (довжинаX - 1))
  );
}
