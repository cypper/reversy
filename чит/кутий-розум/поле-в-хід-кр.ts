import { writeFileSync } from "node:fs";
import tf from "./tf";
import { БгалКР, УявнийКутийРозум } from "./уявний-кр";
import { ДаніКРСлужба } from "./дані-служба";
import { Поле, СтанКлітинки } from "../гра/поле";
import { Гра, Події } from "../гра/гра";
import {
  ГравецьАбиДе,
  ГравецьАбиКрай,
  ГравецьАбиКраїТаПеревернутиБільше,
  ГравецьАбиПеревернутиБільше,
  ГравецьКР,
} from "../гра/гравці";
import { однобіжно, перемішати, перетвар } from "../обробні";
import { ручитиИснуванняПутіФайлаОБ } from "../файловий-сустрій";
import { провестиВипадковуЛюдськуГру } from "../дані/людські-ігри";

type ВхДані = Поле["поле"];
type ВихДані = number; // -1 (білі) - +1(чорні)

type ТварВхДаних = tf.Rank.R3;
type ТварВихДаних = tf.Rank.R1;

type ВхДаніКР = tf.Tensor<ТварВхДаних>;
type ВихДаніКР = tf.Tensor<ТварВихДаних>;

export class ПолеВХідКутийРозум extends УявнийКутийРозум<
  ВхДані,
  ВихДані,
  ВхДаніКР,
  ВихДаніКР
> {
  readonly розмірВхіднихДаних: number;
  readonly тварВхіднихДаних: tf.ShapeMap[ТварВхДаних];
  readonly розмірВихіднихДаних: number;
  readonly тварВихіднихДаних: tf.ShapeMap[ТварВихДаних];
  readonly пострійЧовпення: Partial<
    tf.ModelFitArgs & tf.ModelFitDatasetArgs<{ xs: tf.Tensor; ys: tf.Tensor }>
  >;

  constructor(
    private readonly даніКРСлужба: ДаніКРСлужба,
    private readonly довжинаПоляX: number,
    private readonly довжинаПоляY: number,
    verbose: 0 | 1 = 1,
  ) {
    super();

    this.пострійЧовпення = {
      verbose,
    };

    this.розмірВхіднихДаних = даніКРСлужба.полеДані(
      this.довжинаПоляX,
      this.довжинаПоляY,
    ).розмір;
    this.тварВхіднихДаних = даніКРСлужба.полеДані(
      this.довжинаПоляX,
      this.довжинаПоляY,
    ).твар;

    this.розмірВихіднихДаних = 1;
    this.тварВихіднихДаних = [1];
  }

  створитиБгал() {
    const бгал = tf.sequential();
    бгал.add(
      tf.layers.inputLayer({
        inputShape: this.тварВхіднихДаних,
      }),
    );
    бгал.add(
      tf.layers.reshape({
        targetShape: [this.розмірВхіднихДаних],
      }),
    );
    // бгал.add(
    //   tf.layers.dense({
    //     units: Math.round(this.розмірВхіднихДаних),
    //     activation: "elu",
    //   }),
    // );
    бгал.add(
      tf.layers.dense({
        units: Math.round(this.розмірВхіднихДаних / 2),
        activation: "swish",
      }),
    );
    бгал.add(
      tf.layers.dense({
        units: Math.round(this.розмірВхіднихДаних / 4),
        activation: "swish",
      }),
    );
    // бгал.add(
    //   tf.layers.dropout({ rate: 0.2 }),
    // );
    // бгал.add(
    //   tf.layers.dense({
    //     units: Math.round(this.розмірВхіднихДаних / 3),
    //     activation: "elu",
    //   }),
    // );
    бгал.add(
      tf.layers.dense({
        units: this.розмірВихіднихДаних,
        activation: `tanh`,
      }),
    );
    бгал.add(
      tf.layers.reshape({
        targetShape: this.тварВихіднихДаних,
      }),
    );

    this.укласти(бгал);

    return бгал;
  }

  private укласти(бгал: БгалКР) {
    // бгал.compile({ loss: 'meanSquaredError', optimizer: 'sgd' });
    бгал.compile({
      // loss: (исть: tf.Tensor, пит: tf.Tensor) => {
      //   исть.print()
      //   console.log(исть.shape)
      //   // пит.print()
      //   // console.log(пит.shape)
      //   // исть.sub(исть.mul(пит)).print()

      //   return tf.sum(исть.sub(исть.mul(пит)), -1);
      // },
      // loss: "meanSquaredError",
      // loss: tf.losses.absoluteDifference,
      loss: tf.losses.huberLoss,
      // optimizer: "sgd",
      // optimizer: tf.train.adadelta(0.01),
      optimizer: tf.train.adamax(0.01),
      // optimizer: tf.train.adagrad(0.001),
      // optimizer: tf.train.momentum(0.01, 0.9, true),
      // optimizer: tf.train.rmsprop(0.01),
      metrics: [
        // "accuracy",
        // tf.metrics.categoricalAccuracy,
        // tf.metrics.categoricalCrossentropy,
        // tf.metrics.binaryCrossentropy,
        // tf.metrics.binaryAccuracy,
        // function точність(исть: tf.Tensor, пит: tf.Tensor) {
        //   return tf.logicalAnd(
        //     исть.mul(пит).max(1).max(1).equal(пит.max(1).max(1)),
        //     пит.max(1).max(1).notEqual(0),
        //   );
        // },
      ],
    });
  }

  async створитиЧовпніДаніКР(): Promise<void> {
    ручитиИснуванняПутіФайлаОБ(
      "./кутий-розум-човпне/човпні-дані-ймовірні-ходи.json",
    );
    writeFileSync(
      "./кутий-розум-човпне/човпні-дані-ймовірні-ходи.json",
      JSON.stringify(await this.витягЙмовірнихХодів()),
    );
  }

  private async провестиЗмагання(бгал?: БгалКР, події?: Події) {
    const гра = new Гра(this.довжинаПоляX, this.довжинаПоляY);

    const гр1КР = new ГравецьКР(
      this.довжинаПоляX,
      this.довжинаПоляY,
      undefined,
      0,
      бгал,
    );
    const гр2КР = new ГравецьКР(
      this.довжинаПоляX,
      this.довжинаПоляY,
      undefined,
      0,
      бгал,
    );

    const гравці1 = [
      гр1КР,
      // new ГравецьАбиПеревернутиБільше(),
      // new ГравецьАбиКраїТаПеревернутиБільше(),
      // new ГравецьАбиДе(),
      // new ГравецьАбиКрай(),
    ];
    const гравці2 = [
      гр2КР,
      new ГравецьАбиПеревернутиБільше(),
      new ГравецьАбиКраїТаПеревернутиБільше(),
      new ГравецьАбиДе(),
      new ГравецьАбиКрай(),
    ];

    const [гравець1, гравець2] = [
      перемішати([...гравці1])[0],
      перемішати([...гравці2])[0],
    ];

    return await гра.провестиГру(гравець1, гравець2, події);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  private async витягЙмовірнихХодів(бгал?: БгалКР) {
    const дані: [ВхДані, ВихДані][] = [];

    // console.time("дані");
    for (let i = 0; i < 1000; i++) {
      const події: Події = {
        // хід: (поле, станГравця, хід, ймовірніХоди, рядницяПеревертівГравця) => {
        //   (станГравця === СтанКлітинки.ЧОРНЕ ? даніГравця1 : даніГравця2).push([
        //     поле.ціглийТезярСтануПоля() + String(станГравця),
        //     [
        //       станГравця,
        //       structuredClone(поле.поле),
        //       ймовірніХоди,
        //       рядницяПеревертівГравця,
        //     ],
        //     хід,
        //   ]);
        // },
      };

      const { даніХодів, станПереможця } =
        Math.random() > 0
          ? await провестиВипадковуЛюдськуГру(події)
          : await this.провестиЗмагання(бгал, події);

      for (const [числоХоду, даніХоду] of даніХодів.entries()) {
        const змінаКлітинокПоХодах = [даніХоду.кількістьПеревернутих];
        for (let настВк = числоХоду + 1; настВк < даніХодів.length; настВк++) {
          const настДаніХоду = даніХодів[настВк];
          змінаКлітинокПоХодах.push(
            (даніХоду.станГравця === настДаніХоду.станГравця ? 1 : -1) *
              настДаніХоду.кількістьПеревернутих,
          );
        }

        let хорошість = 0;

        // -15 ~ 15
        let хорошістьЗміни = 0;
        for (const [вк, зміна] of змінаКлітинокПоХодах.entries()) {
          // const тяжістьЗміни = 1 - Math.sqrt(вк / змінаКлітинокПоХодах.length);
          const тяжістьЗміни =
            1 - Math.pow(вк / змінаКлітинокПоХодах.length, 1 / 8);
          хорошістьЗміни += зміна * тяжістьЗміни;
        }
        // ймовірностіХодів[даніХоду.хід.x][даніХоду.хід.y] += перетвар(хорошістьХоду, -15, 15, -0.25, 0.25);

        // -15 ~ 15
        хорошість += хорошістьЗміни;

        // -3 ~ 3
        хорошість +=
          ((даніХоду.станГравця === СтанКлітинки.ЧОРНЕ ? 1 : -1) *
            даніХоду.ймовірніХоди.length) /
          10;

        // -32 - 32
        хорошість +=
          ((даніХоду.станГравця === СтанКлітинки.ЧОРНЕ ? 1 : -1) *
            (даніХоду.кількістьКлітинокГравцяДоХоду -
              даніХоду.кількістьКлітинокСуперникаДоХоду)) /
          2;

        // -5 - 5
        хорошість += (даніХоду.станГравця === станПереможця ? 1 : -1) * 5;

        // -64 - 64
        // хорошість += (даніХоду.станГравця === СтанКлітинки.ЧОРНЕ ? 1 : -1) * (даніХоду.рядницяПеревертівГравця.reduce((гал, ряд) => Math.max(ряд.map((переверти) => переверти.length)), 0))

        дані.push([
          даніХоду.полеДоХоду,
          перетвар(хорошість, -(15 + 3 + 32 + 5), 15 + 30 + 32 + 5, -1, 1),
        ]);
      }
    }

    // console.log(
    //   "lenght ",
    //   дані.length,
    // );
    // console.timeEnd("дані");

    return перемішати(дані);
  }

  async витягЧовпнийНабірДанихКР(бгал?: БгалКР) {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const that = this;

    // const data = await that.витягЙмовірнихХодів();
    const data = null;
    // const data = JSON.parse(
    //   readFileSync(
    //     "./кутий-розум-човпне/човпні-дані-ймовірні-ходи.json",
    //     "utf-8",
    //   ),
    // ) as Awaited<ReturnType<typeof that.витягЙмовірнихХодів>>;

    const створювач = async function* () {
      let ходи = data ?? (await that.витягЙмовірнихХодів(бгал));
      for (let i = 0; i < 2; i++) {
        if (ходи.length === 0) {
          console.error("Помилка");
        }
        for (const дані of ходи) {
          yield {
            xs: await that.вВхДаніКР(дані[0]),
            ys: await that.вВихДаніКР(дані[1]),
          };
        }

        if (i === 1) {
          i = 0;
          ходи = data ?? (await that.витягЙмовірнихХодів(бгал));
        }
      }
    } as unknown as () => Generator<{ xs: ВхДаніКР; ys: ВихДаніКР }>;

    return tf.data
      .generator<{ xs: ВхДаніКР; ys: ВихДаніКР }>(створювач)
      .batch(32 * 2) as tf.data.Dataset<{ xs: tf.Tensor; ys: tf.Tensor }>;
  }

  async витягЧовпнийРядДанихКР() {
    const дані = await this.витягЙмовірнихХодів();
    const човпніДані = await однобіжно(дані, async (д) => {
      return [await this.вВхДаніКР(д[0]), await this.вВихДаніКР(д[1])] as const;
    });

    return {
      многоВхДаніКР: човпніДані.map((д) => д[0]),
      многоВихДаніКР: човпніДані.map((д) => д[1]),
    };
  }

  async човпити(
    бгал: БгалКР,
    рядАбоНабірДаних:
      | { многоВхДаніКР: ВхДаніКР[]; многоВихДаніКР: ВихДаніКР[] }
      | tf.data.Dataset<{ xs: tf.Tensor; ys: tf.Tensor }>,
    волі?: { крятПоступу?: tf.CustomCallbackArgs },
  ) {
    const навчань = 2;
    const прогонів = 100;
    const пакетівВПрогоні = 500;

    if (рядАбоНабірДаних instanceof tf.data.Dataset) {
      for (let i = 0; i < навчань; i++) {
        await бгал.fitDataset(рядАбоНабірДаних, {
          epochs: прогонів,
          batchesPerEpoch: пакетівВПрогоні,
          callbacks: волі?.крятПоступу,
          ...this.пострійЧовпення,
        });
        console.log(i + 1, "з", навчань);
      }
    } else {
      const злученіВхДаніКР = tf.tensor(
        tf.stack(рядАбоНабірДаних.многоВхДаніКР).arraySync(),
        [рядАбоНабірДаних.многоВхДаніКР.length, ...this.тварВхіднихДаних],
      );
      const злученіВихДаніКР = tf.tensor(
        tf.stack(рядАбоНабірДаних.многоВихДаніКР).arraySync(),
        [рядАбоНабірДаних.многоВихДаніКР.length, ...this.тварВихіднихДаних],
      );

      await бгал.fit(злученіВхДаніКР, злученіВихДаніКР, {
        epochs: прогонів,
        shuffle: true,
        batchesPerEpoch: пакетівВПрогоні,
        callbacks: волі?.крятПоступу,
        ...this.пострійЧовпення,
      });
    }
  }

  віщуватиКР(бгал: БгалКР, вхДані: ВхДаніКР): ВихДаніКР {
    const віщаКР = бгал.predict(вхДані.as4D(1, ...вхДані.shape)) as ВихДаніКР;
    if (Array.isArray(віщаКР)) throw new Error("Неочікуваний масив");
    return віщаКР.as1D();
  }

  async віщувати(бгал: БгалКР, вхДані: ВхДані): Promise<ВихДані> {
    const вихДаніКР = this.віщуватиКР(бгал, await this.вВхДаніКР(вхДані));
    // вихДаніКР.print();
    return this.зВихДанихКР(вихДаніКР);
  }

  async вВхДаніКР(дані: ВхДані) {
    //   console.log(
    //     вихДаніВВивід(
    //       tf.tensor(спитДані[0][1].flat().flat(), [1, this.розмірВихіднихДаних]),
    //     ),
    //   );
    return tf.tensor<ТварВхДаних>(
      this.даніКРСлужба.полеВДаніКР(дані),
      this.тварВхіднихДаних,
    );
  }

  async вВихДаніКР(дані: ВихДані) {
    //   console.log(
    //     вихДаніВВивід(
    //       tf.tensor(спитДані[0][1].flat().flat(), [1, this.розмірВихіднихДаних]),
    //     ),
    //   );
    // return tf.tensor1d(this.даніКРСлужба.частинаМовиВДаніКР(дані));
    // tf.tensor2d(дані).print()
    // tf.softmax(tf.tensor2d(дані).as1D().mul(10)).reshape(this.тварВихіднихДаних).print()
    // return tf
    //   .softmax(tf.tensor2d(дані).as1D().mul(10))
    //   .as2D(дані.length, дані[0].length);
    return tf.tensor1d([дані]);
  }

  async зВихДанихКР(вихДаніКР: ВихДаніКР) {
    return вихДаніКР.arraySync()[0];
  }

  async ввізБгалу(путьПапки: string): Promise<БгалКР> {
    const бгал = await tf.loadLayersModel(`file://${путьПапки}/model.json`);

    this.укласти(бгал);

    return бгал;
  }
}
