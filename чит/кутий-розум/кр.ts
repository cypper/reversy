// import { createInterface } from 'readline';
import { УвидженняСлужба } from "./увидження-служба";
// import { Поле, СтанКлітинки } from '../гра/поле';
import { БгалКР, УявнийКутийРозум } from "./уявний-кр";

const увидженняСлужба = new УвидженняСлужба();

// await увидженняСлужба.започаткуватиЛибач();
увидженняСлужба.запуститиTfboard();

export async function створитиЧовпніДані<T1, T2, T3, T4>(
  кр: УявнийКутийРозум<T1, T2, T3, T4>,
) {
  await кр.створитиЧовпніДаніКР();
}

export async function човпити<T1, T2, T3, T4>(
  кр: УявнийКутийРозум<T1, T2, T3, T4>,
  иснуючийБгал?: БгалКР,
) {
  const бгал = иснуючийБгал ?? кр.створитиБгал();
  // const бгал = await кр.ввізБгалу(
  //   './тимч/кр/1706352129944',
  // );

  await увидженняСлужба.відобразитиМережуВішня(бгал);

  const набірДаних = await кр.витягЧовпнийНабірДанихКР(бгал);
  // const рядДаних = await кр.витягЧовпнийРядДанихКР();
  await кр.човпити(бгал, набірДаних, {
    // крятПоступу: await увидженняСлужба.крядДляПоступЧовпенняTfvis()
    крятПоступу: await увидженняСлужба.крядДляПоступЧовпенняTfboard(),
  });

  кр.вивізБгалу("./тимч/кр/" + Date.now().toString(), бгал);

  // await увидженняСлужба.відобразитиМережуВішня(бгал);

  return бгал;
}
