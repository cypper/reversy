import { writeFileSync } from "node:fs";
import tf from "./tf";
import { БгалКР, УявнийКутийРозум } from "./уявний-кр";
import { ДаніКРСлужба } from "./дані-служба";
import { Поле, СтанГравця, СтанКлітинки } from "../гра/поле";
import { Гра, Місце } from "../гра/гра";
import { ГравецьАбиДе } from "../гра/гравці";
import { однобіжно } from "../обробні";
import { ручитиИснуванняПутіФайлаОБ } from "../файловий-сустрій";

type ВхДані = [СтанГравця, Поле["поле"]];
type ВихДані = Місце[];

type ТварВхДаних = tf.Rank.R2;
type ТварВихДаних = tf.Rank.R2;

type ВхДаніКР = tf.Tensor<ТварВхДаних>;
type ВихДаніКР = tf.Tensor<ТварВихДаних>;

export class ПолеВПустіКутийРозум extends УявнийКутийРозум<
  ВхДані,
  ВихДані,
  ВхДаніКР,
  ВихДаніКР
> {
  readonly розмірВхіднихДаних: number;
  readonly тварВхіднихДаних: tf.ShapeMap[ТварВхДаних];
  readonly розмірВихіднихДаних: number;
  readonly тварВихіднихДаних: tf.ShapeMap[ТварВихДаних];
  readonly пострійЧовпення: Partial<
    tf.ModelFitArgs & tf.ModelFitDatasetArgs<{ xs: tf.Tensor; ys: tf.Tensor }>
  >;

  constructor(
    private readonly даніКРСлужба: ДаніКРСлужба,
    private readonly довжинаПоляX: number,
    private readonly довжинаПоляY: number,
    verbose: 0 | 1 = 1,
  ) {
    super();

    this.пострійЧовпення = {
      verbose,
    };

    this.розмірВхіднихДаних = даніКРСлужба.поле2Дані(
      this.довжинаПоляX,
      this.довжинаПоляY,
    ).розмір;
    this.тварВхіднихДаних = даніКРСлужба.поле2Дані(
      this.довжинаПоляX,
      this.довжинаПоляY,
    ).твар;
    this.розмірВихіднихДаних = даніКРСлужба.хідДані(
      this.довжинаПоляX,
      this.довжинаПоляY,
    ).розмір;
    this.тварВихіднихДаних = даніКРСлужба.хідДані(
      this.довжинаПоляX,
      this.довжинаПоляY,
    ).твар;
  }

  створитиБгал() {
    const бгал = tf.sequential();
    бгал.add(
      tf.layers.inputLayer({
        inputShape: this.тварВхіднихДаних,
      }),
    );
    бгал.add(
      tf.layers.reshape({
        targetShape: [this.розмірВхіднихДаних],
      }),
    );
    бгал.add(
      tf.layers.dense({
        units: Math.round(this.розмірВхіднихДаних),
        activation: "selu",
      }),
    );
    бгал.add(
      tf.layers.dense({
        units: Math.round(this.розмірВхіднихДаних),
        activation: "selu",
      }),
    );
    бгал.add(
      tf.layers.dense({
        units: this.розмірВихіднихДаних,
        activation: `sigmoid`,
        // kernelInitializer: 'zeros',
        // biasInitializer: 'zeros',
      }),
    );
    бгал.add(
      tf.layers.reshape({
        targetShape: this.тварВихіднихДаних,
      }),
    );

    this.укласти(бгал);

    return бгал;
  }

  private укласти(бгал: БгалКР) {
    // бгал.compile({ loss: 'meanSquaredError', optimizer: 'sgd' });
    бгал.compile({
      // loss: (исть: tf.Tensor, пит: tf.Tensor) => {
      //   // исть.print()
      //   // console.log(исть.shape)
      //   // пит.print()
      //   // console.log(пит.shape)
      //   // исть.sub(исть.mul(пит)).print()

      //   return tf.sum(исть.sub(исть.mul(пит)), -1);
      // },
      // loss: "meanSquaredError",
      loss: tf.losses.sigmoidCrossEntropy,
      // optimizer: "sgd",
      optimizer: tf.train.adamax(0.01),
      // optimizer: tf.train.momentum(0.1, 0.9),
      metrics: [
        "accuracy",
        tf.metrics.binaryAccuracy,
        tf.metrics.binaryCrossentropy,
      ],
    });
  }

  async створитиЧовпніДаніКР(): Promise<void> {
    ручитиИснуванняПутіФайлаОБ(
      "./кутий-розум-човпне/човпні-дані-ймовірні-ходи.json",
    );
    writeFileSync(
      "./кутий-розум-човпне/човпні-дані-ймовірні-ходи.json",
      JSON.stringify(await this.витягЙмовірнихХодів()),
    );
  }

  private async витягЙмовірнихХодів() {
    const гра = new Гра(this.довжинаПоляX, this.довжинаПоляY);
    const дані: [ВхДані, ВихДані][] = [];
    console.time("гра");
    for (let i = 0; i <= 1000; i++) {
      await гра.провестиГру(new ГравецьАбиДе(), new ГравецьАбиДе(), {
        оновленнеПоле: (поле) => {
          дані.push([
            [
              Math.random() < 0.5 ? СтанКлітинки.ЧОРНЕ : СтанКлітинки.БІЛЕ,
              structuredClone(поле.поле),
            ],
            поле.пустіКлітинки(),
          ]);
        },
      });
    }
    console.log("lenght ", дані.length);
    console.timeEnd("гра");

    return дані;
  }

  async витягЧовпнийНабірДанихКР() {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const that = this;

    const data = await that.витягЙмовірнихХодів();

    const створювач = async function* () {
      for (const дані of data) {
        yield {
          xs: await that.вВхДаніКР(дані[0]),
          ys: await that.вВихДаніКР(дані[1]),
        };
        // console.log('---', дані[0][1]. поле, дані[1])
        // a.xs.print()
        // a.ys.print()
        // console.log('---e')

        // yield a
      }
    } as unknown as () => Generator<{ xs: ВхДаніКР; ys: ВихДаніКР }>;

    return tf.data
      .generator<{ xs: ВхДаніКР; ys: ВихДаніКР }>(створювач)
      .batch(32) as tf.data.Dataset<{ xs: tf.Tensor; ys: tf.Tensor }>;
  }

  async витягЧовпнийРядДанихКР() {
    const дані = await this.витягЙмовірнихХодів();
    const човпніДані = await однобіжно(дані, async (д) => {
      return [await this.вВхДаніКР(д[0]), await this.вВихДаніКР(д[1])] as const;
    });

    return {
      многоВхДаніКР: човпніДані.map((д) => д[0]),
      многоВихДаніКР: човпніДані.map((д) => д[1]),
    };
  }

  async човпити(
    бгал: БгалКР,
    рядАбоНабірДаних:
      | { многоВхДаніКР: ВхДаніКР[]; многоВихДаніКР: ВихДаніКР[] }
      | tf.data.Dataset<{ xs: tf.Tensor; ys: tf.Tensor }>,
    волі?: { крятПоступу?: tf.CustomCallbackArgs },
  ) {
    const прогонів = 100;

    if (рядАбоНабірДаних instanceof tf.data.Dataset) {
      await бгал.fitDataset(рядАбоНабірДаних, {
        epochs: прогонів,
        callbacks: волі?.крятПоступу,
        ...this.пострійЧовпення,
      });
    } else {
      const злученіВхДаніКР = tf.tensor(
        tf.stack(рядАбоНабірДаних.многоВхДаніКР).arraySync(),
        [рядАбоНабірДаних.многоВхДаніКР.length, ...this.тварВхіднихДаних],
      );
      const злученіВихДаніКР = tf.tensor(
        tf.stack(рядАбоНабірДаних.многоВихДаніКР).arraySync(),
        [рядАбоНабірДаних.многоВихДаніКР.length, ...this.тварВихіднихДаних],
      );

      await бгал.fit(злученіВхДаніКР, злученіВихДаніКР, {
        epochs: прогонів,
        shuffle: true,
        callbacks: волі?.крятПоступу,
        ...this.пострійЧовпення,
      });
    }
  }

  віщуватиКР(бгал: БгалКР, вхДані: ВхДаніКР): ВихДаніКР {
    const віщаКР = бгал.predict(вхДані.as3D(1, ...вхДані.shape)) as ВихДаніКР;
    if (Array.isArray(віщаКР)) throw new Error("Неочікуваний масив");
    return віщаКР.as2D(this.довжинаПоляX, this.довжинаПоляY);
  }

  async віщувати(бгал: БгалКР, вхДані: ВхДані): Promise<ВихДані> {
    const вихДаніКР = this.віщуватиКР(бгал, await this.вВхДаніКР(вхДані));
    // вихДаніКР.print();
    return this.зВихДанихКР(вихДаніКР);
  }

  async вВхДаніКР(дані: ВхДані) {
    //   console.log(
    //     вихДаніВВивід(
    //       tf.tensor(спитДані[0][1].flat().flat(), [1, this.розмірВихіднихДаних]),
    //     ),
    //   );
    return tf.tensor2d(this.даніКРСлужба.поле2ВДаніКР(...дані));
  }

  async вВихДаніКР(дані: ВихДані) {
    //   console.log(
    //     вихДаніВВивід(
    //       tf.tensor(спитДані[0][1].flat().flat(), [1, this.розмірВихіднихДаних]),
    //     ),
    //   );
    // return tf.tensor1d(this.даніКРСлужба.частинаМовиВДаніКР(дані));
    return tf.tensor2d(
      this.даніКРСлужба.ходиВДаніКР(дані, this.довжинаПоляX, this.довжинаПоляY),
    );
    // return tf.tensor1d(дані ? [1] : [0]);
  }

  async зВихДанихКР(вихДаніКР: ВихДаніКР) {
    return this.даніКРСлужба.ходиЗДанихКР(вихДаніКР.arraySync());
  }

  async ввізБгалу(путьПапки: string) {
    const бгал = await tf.loadLayersModel(`file://${путьПапки}/model.json`);

    this.укласти(бгал);

    return бгал;
  }
}
