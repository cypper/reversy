class Вузол<T> {
  constructor(
    public readonly item: T,
    public next: Вузол<T> | null,
  ) {}
}

export class ЧередаВузлом<T> {
  tailN: Вузол<T> | null = null;
  headN: Вузол<T> | null = null;

  вчередити(el: T) {
    const newTail = new Вузол(el, null);
    if (this.tailN) this.tailN.next = newTail;
    if (this.чиПуста()) this.headN = newTail;
    this.tailN = newTail;
  }
  вичередити() {
    const head = this.headN;
    this.headN = this.headN?.next ?? null;
    if (this.headN === null) this.tailN = null;
    return head?.item ?? null;
  }
  head() {
    return this.headN?.item ?? null;
  }
  чиПуста() {
    return this.headN === null;
  }
  len() {
    let len = 0,
      cur = this.headN;
    while (cur) {
      len++;
      cur = cur.next;
    }
    return len;
  }
  toArr() {
    const arr: T[] = [];
    let cur = this.headN;
    while (cur) {
      arr.push(cur.item);
      cur = cur.next;
    }
    return arr;
  }
}
