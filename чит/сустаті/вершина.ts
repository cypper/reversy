export class Вершина<З> {
  constructor(
    public ключ: string,
    public значення: З,
    public тяж: number,
    private входи: Record<Вершина<З>["ключ"], true>,
    private виходи: Record<Вершина<З>["ключ"], number>,
  ) {}

  додатиВхід(ключ: Вершина<З>["ключ"]) {
    if (this.входи[ключ] !== undefined) throw new Error("Даний вхід уже иснує");

    this.входи[ключ] = true;
  }

  видалитиВхід(ключ: Вершина<З>["ключ"]) {
    if (!this.входи[ключ]) throw new Error("Входу не иснує");

    delete this.входи[ключ];
  }

  додатиВихід(ключ: Вершина<З>["ключ"], натяжжя: number) {
    if (this.виходи[ключ] !== undefined)
      throw new Error("Даний вихід уже иснує");
    this.виходи[ключ] = натяжжя;
  }

  видалитиВихід(ключ: Вершина<З>["ключ"]) {
    if (!this.виходи[ключ]) throw new Error("Виходу не иснує");

    delete this.виходи[ключ];
  }

  виходиКлючі() {
    return Object.keys(this.виходи);
  }

  входиКлючі() {
    return Object.keys(this.входи);
  }
}
